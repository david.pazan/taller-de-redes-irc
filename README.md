# Taller De Redes IRC

Dockerfiles para montar un cliente y servidor IRC con la finalidad de generar y analizar el trafico IRC generado por los estos.
Autores: Matías Ringeling - David Pazán

## Clonar repositorio
En primer lugar se ha de clonar el repositorio, y se procede a instalar el servidor y cliente
```
git https://gitlab.com/david.pazan/taller-de-redes-irc.git
```

## Instalar servidor

```
sudo docker build -t servidor taller-de-redes-irc/Servidor/
```

## Instalar cliente

```
sudo docker build -t cliente taller-de-redes-irc/Cliente/

```
# Uso

A continuación los comandos para ejecutar los docker.

```
sudo docker run -it -p 6667:6667 --name servidor_irc servidor
```

```
sudo docker run -it --name cliente_irc cliente

```
